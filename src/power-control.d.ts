declare module '@modules/power-control' {
  class Power {
    turnOn(): void;
    turnOff(): void;
    toggle(on?: boolean): void;
    isOn(): boolean;
    blink(onTime: number, offTime?: number): void;

    power(): number;
    power(value: number): void;
  }

  function connect(pin: Pin): Power;
}

export {}