import { ObjectConstructor } from '@tether_toolkit/types';

type Event = 'press' | 'release' | 'click' | 'hold'

export interface IButton extends ObjectConstructor {
  /**
   * Returns state
   */
  isPressed(): boolean;
}

declare module '@modules/button' {
  interface IConnectOptions {
    /**
     * Time in seconds to trigger hold event
     */
    holdTime?: number;
    /**
     * Pass 0 to revert click event moment to release event
     */
    normalSignal?: 0 | 1;
    /**
     * Debounce time in seconds
     */
    debounce?: number;
  }

  /**
   * Creates Button entity connected to pin
   */
  export const connect: (pin: Pin, options?: IConnectOptions) => IButton;
}