declare module '@modules/water-level' {
  type WaterLevelStateDown = 'down';
  type WaterLevelStateUp = 'up';
  type WaterLevelState = WaterLevelStateDown | WaterLevelStateUp;

  class WaterLevel {
    read(): WaterLevelState;
    on(event: WaterLevelState, cb: () => unknown);
  }

  function connect(
    pin: Pin,
    options?: Partial<{ debounce: number; mounted: boolean }>
  ): WaterLevel;
}

export {}