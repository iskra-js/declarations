declare module '@modules/wifi' {
  type MaybeNull<T> = T | null;
  type ErrorCallback = (err: MaybeNull<Error>) => void;
  type ESP8266Encoding =
    | 'open'
    | 'wep'
    | 'wpa_psk'
    | 'wpa2_psk'
    | 'wpa_wpa2_psk';
  type ESP8266ConnectedDevice = { ip: string; mac: string };
  type ESP8266AP = {
    ssid: string;
    enc: ESP8266Encoding;
    signal: number;
    mac: string;
  };

  class ESP8266 {
    /**
     * Restart module.
     * @param cb - Callback for error handling
     */
    reset(cb: ErrorCallback): void;

    /**
     * Get module firmware version.
     * @param cb - Callback for handling error or version
     */
    getVersion<T = string>(
      cb: (err: MaybeNull<Error>, version: T) => unknown
    ): void;

    /**
     * Connect to wifi-network.
     * @param ssid - Network SSID
     * @param key - Network password
     * @param cb - Callback for error handling
     */
    connect(ssid: string, key: string, cb: ErrorCallback): void;

    /**
     * Get list of wifi-networks.
     * @param cb - Callback for handling error and data
     */
    getAPs(cb: (err: MaybeNull<Error>, aps: ESP8266AP[]) => unknown): void;

    /**
     * Create wifi-network.
     * @param ssid - Network SSid
     * @param key - Network password
     * @param channel - Channel number
     * @param enc - Encoding type
     * @param cb - Callback for error handling
     */
    createAP(
      ssid: string,
      key: string,
      channel: number,
      enc: ESP8266Encoding,
      cb: ErrorCallback
    ): void;

    /**
     * Get list of connected devices created by createAP method.
     * @param cb - Callback for handling error and data
     */
    getConnectedDevices(
      cb: (err: MaybeNull<Error>, devs: ESP8266ConnectedDevice[]) => unknown
    ): void;

    /**
     * Get IP address of module connected by connect method.
     * @param cb - Callback for handling error and data
     */
    getIP(cb: (err: MaybeNull<Error>, ip: string) => unknown): void;

    /**
     * Subscribe on error event.
     * @param event - Event name
     * @param cb - Callback for handling error
     */
    on(
      event: 'err',
      cb: (err: 'CIPSERVER' | 'No free sockets' | 'CIPSTART') => unknown
    ): void;
  }

  /**
   * Setup ESP8266 entity.
   */
  export const setup: (pin: Serial, cb: ErrorCallback) => ESP8266;
}
