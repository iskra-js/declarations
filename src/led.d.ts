export interface ILed {
  /**
   * Turns led on
   */
  turnOn(): void;
  /**
   * Turns led off
   */
  turnOff(): void;
  /**
   * Sets up passed state or toggle it
   */
  toggle(state?: boolean): void;
  /**
   * Returns state
   */
  isOn(): boolean;
  /**
   * Sets up blink mode. If there is not offTime param, then LED will blink once
   * @param onTime - flash on interval
   * @param offTime - flash of interval
   */
  blink(onTime: number, offTime?: number);
  /**
   * Sets up brightness
   * @param value - brightness value between 0 and 1
   */
  brightness(value: number);
}

declare module '@modules/led' {
  /**
   * Creates Led entity connected to pin
   */
  export const connect: (pin: Pin) => ILed;
}